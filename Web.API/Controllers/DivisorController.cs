﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace Web.API.Controllers
{
    [Route("api/[controller]")]
    public class DivisorController : Controller
    {
        /// <summary>
        /// Given the set [1, 1, 1, 3, 5, 9] complete the sequence to 11 numbers
        /// Get the nTH instance of the sequence where (n / x) has no remainder
        /// </summary>
        /// <param name="x"></param>
        /// <param name="nth">the nth count</param>
        /// <returns></returns>
        [HttpGet]
        public int Get(int x, int nth)
        {
            throw new NotImplementedException("solve the algorithm");
        }
    }
}
