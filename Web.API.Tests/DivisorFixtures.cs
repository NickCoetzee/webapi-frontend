using Microsoft.VisualStudio.TestTools.UnitTesting;
using Web.API.Controllers;

namespace Web.API.Tests
{
    [TestClass]
    public class DivisorFixtures
    {
        [TestMethod]
        public void When_x_Is_3_And_nth_Is_1_Then_Result_is_3()
        {
            var nthNumber = new DivisorController().Get(3, 1);
            Assert.AreEqual(3, nthNumber);
        }

        [TestMethod]
        public void When_x_Is_3_And_nth_Is_2_Then_Result_is_9()
        {
            var nthNumber = new DivisorController().Get(3, 2);
            Assert.AreEqual(9, nthNumber);
        }

        [TestMethod]
        public void When_x_Is_5_And_nth_Is_1_Then_Result_is_5()
        {
            var nthNumber = new DivisorController().Get(5, 1);
            Assert.AreEqual(5, nthNumber);
        }

        [TestMethod]
        public void When_x_Is_5_And_nth_Is_2_Then_Result_is_105()
        {
            var nthNumber = new DivisorController().Get(5, 2);
            Assert.AreEqual(105, nthNumber);
        }
    }
}
